rm -rf out
mkdir out
riscv64-unknown-elf-gcc -w -g -mcmodel=medany -static -std=gnu99 -fno-builtin-printf -c main.c -o out/main.o
riscv64-unknown-elf-gcc -march=rv64imafd  -mcmodel=medany -static -std=gnu99 -fno-common -fno-builtin-printf -D__ASSEMBLY__=1 -c crt.S -o out/crt.o
riscv64-unknown-elf-gcc -march=rv64imafd  -mcmodel=medany -static -std=gnu99 -fno-common -fno-builtin-printf -c syscalls.c -o out/syscalls.shakti
riscv64-unknown-elf-gcc -T ./link.ld out/main.o out/crt.o out/syscalls.shakti -o out/main.elf -static -mcmodel=medany -nostartfiles -lm -lgcc  
riscv64-unknown-elf-objdump -D out/main.elf > out/main.dump
