This repository contains a basic snake and ladder game compiled on RISCV gnu toolchain and tested on 64 bit single core SHAKTI processor

Source Files: main.c 
              crt.S 
              syscalls.c
              util.h
              encoding.h

Linker script: link.ld

Makefile: script_main.sh

    
Follow these steps to generate the executable: 

    $ git clone https://gitlab.com/shaktiproject/software/shakti-game.git
    $ cd shakti-game
    $ ./script_main.sh
